from expects import *
import mamba
import index 

with description('FizzBuzz') as self:
  
  with it('if the number is not divisible by 5 or 3, write the number you give'):
    # Arrange-Arreglo
    fizzBuzz = index.FizzBuzz()
    # Act
    result = fizzBuzz.calculate(2)
    # Assert
    expect(result).to(equal('2'))

  with it('if the number is divisible by 3, write Fizz'):
    fizzBuzz = index.FizzBuzz()
    # fizzNumber = 3
    result = fizzBuzz.calculate(9)

    expect(result).to(equal('Fizz'))

  with it('if the number is divisible by 5, write Buzz'):
    fizzBuzz = index.FizzBuzz()

    result = fizzBuzz.calculate(20)

    expect(result).to(equal('Buzz'))

  with it('if the number is divisble by 3 anf 5, write FizzBuzz'):
    fizzBuzz = index.FizzBuzz()

    result = fizzBuzz.calculate(30)

    expect(result).to(equal('FizzBuzz'))



